import * as PIXI from 'pixi.js';
import { GameScene } from './gameScene';
import { Sprite } from './sprite';
import { Explorer } from './explorer';

const TextureCache = PIXI.utils.TextureCache;
const resources = PIXI.Loader.shared.resources;
let stage;
let outerBar ,innerBar;
let message;
let buttonUpTexture ;
let buttonDownTexture;
class Game extends PIXI.Application{
    constructor() {
        super();
        document.body.appendChild(this.view);
        this.renderer.backgroundColor = 0x061639;
        this.renderer.autoDensity = true;
        this.renderer.resize(512, 512);
    }
    load(){
        this.loader.add('./img/treasureHunter.json');
        this.loader.add('./img/buttonGame.json');
        this.loader.load(this.setup.bind(this));
    }
    setup(){
        this.gameScene1 = new GameScene();
        this.stage.addChild(this.gameScene1);
        this.gameScene1.setVisible(true)

        this.dungeon = new Sprite(TextureCache["dungeon.png"]);
        this.gameScene1.addChild(this.dungeon);

        this.explorer = new Explorer(TextureCache["explorer.png"]);
        this.explorer.setPosition(35,32);
        this.explorer.setVelocity(0,0);
        this.gameScene1.addChild(this.explorer);

        this.door = new Sprite(TextureCache["door.png"])
        this.door.setPosition(32,0);
        this.gameScene1.addChild(this.door);

        this.treasure = new Sprite(TextureCache["treasure.png"]);
        this.treasure.setPosition(this.gameScene1.width - this.treasure.width -48,this.gameScene1.height/2 -this.treasure.height/2);
        this.treasure.setVelocity(0,0);
        this.gameScene1.addChild(this.treasure)

        const numberOfBlobs = 6,
            spacing = 48,
            xOffset = 150;
        this.objBlob =[];
        for (let i = 0; i < numberOfBlobs; i++) {

            //Make a blob
            this.objBlob[i] = new Sprite(TextureCache["blob.png"]);

            //Space each blob horizontally according to the `spacing` value.
            //`xOffset` determines the point from the left of the screen
            //at which the first blob should be added.
            const x = spacing * i + xOffset;

            //Give the blob a random y position
            //(`randomInt` is a custom function - see below)
            const y = this.randomInt(0,  this.gameScene1.height - this.objBlob[i].height  -10);

            //Set the blob's position
            this.objBlob[i].setPosition(x,y);

            this.objBlob[i].setVelocity(0,1);
           
            //Add the blob sprite to the stage
            this.gameScene1.addChild(this.objBlob[i]);
        }
        this.healthBar = new GameScene();
        this.healthBar.position.set(this.gameScene1.width - 170, 4);
        this.gameScene1.addChild(this.healthBar);

        //Create the black background rectangle
        innerBar = new PIXI.Graphics();
        innerBar.beginFill(0x000000);
        innerBar.drawRect(0, 0, 128, 8);
        innerBar.endFill();
        this.healthBar.addChild(innerBar);

        //Create the front red rectangle
        outerBar = new PIXI.Graphics();
        outerBar.beginFill(0xFF3300);
        outerBar.drawRect(0, 0, 128, 8);
        outerBar.endFill();
        this.healthBar.addChild(outerBar);

        this.healthBar.outer = outerBar;

        this.gameOverScene = new GameScene();
        this.stage.addChild(this.gameOverScene);
        this.gameOverScene.setVisible(false);

        this.textAgain = new GameScene();

        buttonUpTexture = TextureCache["button_up.png"];
        buttonDownTexture = TextureCache["button_down.png"];
        this.buttonAgain = new Sprite(buttonUpTexture);
        this.buttonAgain.setPosition(this.stage.width/2 - 128/2, this.stage.height / 2 + 40)
        this.buttonAgain.interactive = true;
        this.buttonAgain.buttonMode = true;
        this.buttonAgain
        // Mouse & touch events are normalized into
        // the pointer* events for handling different
        // button events.
            .on('pointerdown', this.onButtonDownAgain.bind(this))
            .on('pointerup', this.onButtonUp.bind(this))
            .on('pointerupoutside', this.onButtonUp.bind(this))
            .on('pointerover', this.onButtonOver.bind(this))
            .on('pointerout', this.onButtonOut.bind(this));
        this.gameOverScene.addChild(this.buttonAgain)
        
        const space = 3
        this.a1 = new Sprite(TextureCache["a.png"]);
        this.a1.setPosition(this.stage.width/2 - 128/2 + 128/4, this.stage.height / 2 + 42 +12/2)
        this.textAgain.addChild(this.a1);

        this.g = new Sprite(TextureCache["g.png"]);
        this.g.setPosition(this.a1.x + this.a1.width+  space, this.stage.height / 2 + 42 +12/2 )
        this.textAgain.addChild(this.g);

        this.a2 = new Sprite(TextureCache["a.png"]);
        this.a2.setPosition(this.g.x + this.g.width+  space, this.stage.height / 2 + 42 +12/2)
        this.textAgain.addChild(this.a2);

        this.i = new Sprite(TextureCache["i.png"]);
        this.i.setPosition(this.a2.x + this.a2.width+  space, this.stage.height / 2 + 42 +12/2)
        this.textAgain.addChild(this.i);

        this.n = new Sprite(TextureCache["n.png"]);
        this.n.setPosition(this.i.x + this.i.width+  space, this.stage.height / 2 + 42 +12/2)
        this.textAgain.addChild(this.n);

        this.gameOverScene.addChild(this.textAgain)

        const style = new PIXI.TextStyle({
          fontFamily: "Futura",
          fontSize: 64,
          fill: "white"
        });
        message = new PIXI.Text("The End!", style);
        message.x = this.stage.width/2 -200/2;
        message.y = this.stage.height / 2 - 32;
    
        
        this.gameOverScene.addChild(message);

        this.state = this.play;

        this.ticker.add((delta) => this.gameLoop(delta));
    }
    gameLoop(delta){
        this.state(delta);
    }
    play(delta){
        for(let i = 0; i < this.objBlob.length; i++) {
          if(this.detectCollision(this.explorer, this.objBlob[i])){
          
            this.explorer.alpha = 0.5;
            this.healthBar.outer.width -= 1;
          }
          else{
            this.explorer.alpha = 1;
          }
          
          this.objBlob[i].update(delta)
          let blobHitsWall = this.detectCollisionWall(this.objBlob[i]);
          if (blobHitsWall === "top" || blobHitsWall === "bottom") {
            this.objBlob[i].vy *=-1 ;
          }
        }
        if (this.detectCollision(this.explorer, this.treasure)) {
          this.treasure.setPosition(this.explorer.x +8 , this.explorer.y+8);
        }
        let explorerHitsWall = this.detectCollisionWall(this.explorer);
        if(explorerHitsWall === "top" || explorerHitsWall === "bottom"){
          this.explorer.y += 0;
        }else if (explorerHitsWall === "left" || explorerHitsWall === "right") {
          this.explorer.x += 0;
        }
        if (this.hitTestRectangle(this.treasure, this.door)) {
          this.state=this.end;
          message.text = "You won!";
        }
        if (this.healthBar.outer.width < 0) {
          this.state=this.end;
          message.text = "You lost!";
        }
        this.explorer.update(delta);
        this.explorer.control();
    }
    onButtonDownAgain() {
      this.isdown = true;
      this.texture = buttonDownTexture;
      this.alpha = 1;
      this.state = this.againGame;
      
      // this.ticker.remove(this.gameLoop);
      // this.ticker.add((delta) => this.gameLoop(delta));
    }   
    onButtonUp() {
      this.isdown = false;
      this.texture = buttonUpTexture;
    }
    
    onButtonOver() {
      this.isOver = true;
      if (this.isdown) {
          return;
      }
      this.texture = buttonDownTexture;
    }
    
    onButtonOut() {
      this.isOver = false;
      if (this.isdown) {
          return;
      }
      this.texture = buttonUpTexture;
    }

    end() {
      this.gameScene1.setVisible(false);
      this.gameOverScene.setVisible(true)
    }
    againGame(){
      this.gameScene1.setVisible(true);
      this.gameOverScene.setVisible(false)
      this.explorer.setPosition(35,32);
      this.healthBar.outer.width = 128;
      this.treasure.setPosition(this.gameScene1.width - this.treasure.width -48,this.gameScene1.height/2 -this.treasure.height/2);
      this.state = this.play;
    }
    
    detectCollision(obj1, obj2) {
      if(this.hitTestRectangle(obj1,obj2)){
          return true;
      }else
      {
        return false;
      }    
    }
    detectCollisionWall(obj1) {
      return this.contain(obj1, {x: 28, y: 10, width: 488, height: 480});
    }
    
    randomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    hitTestRectangle(r1, r2) {

        //Define the variables we'll need to calculate
        let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;
      
        //hit will determine whether there's a collision
        hit = false;
      
        //Find the center points of each sprite
        r1.centerX = r1.x + r1.width / 2;
        r1.centerY = r1.y + r1.height / 2;
        r2.centerX = r2.x + r2.width / 2;
        r2.centerY = r2.y + r2.height / 2;
      
        //Find the half-widths and half-heights of each sprite
        r1.halfWidth = r1.width / 2;
        r1.halfHeight = r1.height / 2;
        r2.halfWidth = r2.width / 2;
        r2.halfHeight = r2.height / 2;
      
        //Calculate the distance vector between the sprites
        vx = r1.centerX - r2.centerX;
        vy = r1.centerY - r2.centerY;
      
        //Figure out the combined half-widths and half-heights
        combinedHalfWidths = r1.halfWidth + r2.halfWidth;
        combinedHalfHeights = r1.halfHeight + r2.halfHeight;
      
        //Check for a collision on the x axis
        if (Math.abs(vx) < combinedHalfWidths) {
      
          //A collision might be occurring. Check for a collision on the y axis
          if (Math.abs(vy) < combinedHalfHeights) {
      
            //There's definitely a collision happening
            hit = true;
          } else {
      
            //There's no collision on the y axis
            hit = false;
          }
        } else {
      
          //There's no collision on the x axis
          hit = false;
        }
      
        //`hit` will be either `true` or `false`
        return hit;
    };
  
    contain(sprite, container) {
  
        let collision = undefined;
      
        //Left
        if (sprite.x < container.x) {
          sprite.x = container.x;
          collision = "left";
        }
      
        //Top
        if (sprite.y < container.y) {
          sprite.y = container.y;
          collision = "top";
        }
      
        //Right
        if (sprite.x + sprite.width > container.width) {
          sprite.x = container.width - sprite.width;
          collision = "right";
        }
      
        //Bottom
        if (sprite.y + sprite.height > container.height) {
          sprite.y = container.height - sprite.height;
          collision = "bottom";
        }
      
        //Return the `collision` value
        return collision;
  }
}
export {Game}