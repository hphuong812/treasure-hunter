const path = require('path');
const CopyPlugin = require("copy-webpack-plugin");

module.exports={
    entry : './src/function.js',
    mode : 'development',
    output: {
        filename : 'index.js',
        path: path.resolve(__dirname, "dist")
    },
    devServer: {
        static: {
          directory: path.join(__dirname),
        },
        compress: true,
        port: 9000,
      },
    plugins: [
        new CopyPlugin({
            patterns: [
                { from: "img", to: "image" },
            ],
        }),
    ]

}